package Interacrtors;

import java.util.ArrayList;

import Client.GroupListener;

import Entities.*;
import EntitiesNetwork.GroupNetwork;

public class GroupInteractor implements  GroupListener {
    private GroupNetwork groupNetwork;
    private static ArrayList<Group> groups=new ArrayList<Group>();

    GroupInteractor()
    {
        this.groupNetwork= new GroupNetwork(this);
    }


    public void onGetGroups(ArrayList<Group> list)
    {
        this.groups=list;
    }

}
