package Interacrtors;

import java.util.ArrayList;

import Entities.Group;
import Entities.Lecture;
import Client.*;
import Entities.Teacher;
import EntitiesNetwork.GroupNetwork;
import EntitiesNetwork.LectureNetwork;
import EntitiesNetwork.TeacherNetwork;

public class LectureInteractor implements LectureListener{
    private LectureNetwork lectureNetwork;
    private static ArrayList<Lecture> lectures=new ArrayList<Lecture>();

    LectureInteractor()
    {
       this.lectureNetwork= new LectureNetwork(this);
    }

    public void onGetLectures(ArrayList<Lecture> list)
    {
        this.lectures=list;
    }

    public void setLectures() {
        for (int i = 0; i < lectures.size(); i++) {
            Teacher t = TeacherNetwork.getByName(lectures.get(i).getS_teacher());
            Group g = GroupNetwork.getByName(lectures.get(i).getS_teacher());

            lectures.get(i).setTeacher(t);
            lectures.get(i).setGroup(g);
        }
    }

    //получаем лекцию по id
    public Lecture getByIdLecture(int id) {
        for (int i = 0; i < lectures.size(); i++) {
            if (lectures.get(i).getId() == id)
            {
                return lectures.get(id);
            }
            }
            return null;
        }

        //получаем лекцию по преподавателю
        public Lecture getByTeacherLectures(Teacher teacher)
        {
            ArrayList<Lecture> list = new ArrayList<>();
            for (int i=0; i<lectures.size(); i++)
            {
                if (lectures.get(i).getS_teacher()== teacher.toString())
                {
                    list.add(lectures.get(i));
                }
            }

            if (list.isEmpty()) return null;

            for (int i=0; i<lectures.size(); i++)
            {

            }

            return null;
        }
    }