package EntitiesNetwork;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import Entities.*;
import Client.*;

public class GroupNetwork {
    private Group pi161 = new Group();
    private Group pi162 = new Group();
    private Group bi161 = new Group();
    private static ArrayList<Group> groups=new ArrayList<Group>();

    private GroupListener delegate;

    public GroupNetwork(GroupListener delegate) {
        this.delegate = delegate;
    }

    public static Group getByName(String name) {
        for (int i = 0; i < groups.size(); i++) {
            if (groups.get(i).getName() == name)
            {
                return groups.get(i);
            }
        }
        return null;
    }

    public void getGroups(){
       try {
           TimeUnit.SECONDS.sleep(1);
           this.setGroups();
           delegate.onGetGroups(groups);
       }
       catch (InterruptedException ie) {}
    }

    public void setGroups(){
        pi161.setName("ПИ-16-1");
        pi162.setName("ПИ-16-2");
        bi161.setName("БИ-16-1");

        groups.add(pi161);
        groups.add(pi162);
        groups.add(bi161);
    }
}
