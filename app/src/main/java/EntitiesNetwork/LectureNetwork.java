package EntitiesNetwork;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import Entities.*;
import Client.*;

public class LectureNetwork {

    private Lecture APNYA = new Lecture();
    private Lecture TADVI = new Lecture();
    private Lecture NS = new Lecture();
    private static ArrayList<Lecture> lectures=new ArrayList<Lecture>();

    private LectureListener delegate;

    public LectureNetwork(LectureListener delegate) {
        this.delegate = delegate;
    }

    public void getLectures(){
        try {
            TimeUnit.SECONDS.sleep(1);;
            delegate.onGetLectures(lectures);
        }
        catch (InterruptedException ie){}
    }

    public void setLectures(){
        if (!lectures.isEmpty()) {
            APNYA.setId(1);
            APNYA.setName("Академ. письмо на анг.языке");
            APNYA.setDay(2017, 0, 25, 13, 45);
            APNYA.getS_teacher("Туляков");
            APNYA.getS_group("ПИ-16-1");
            TADVI.setId(2);
            TADVI.setName("Технологии анализа данных в Internet");
            TADVI.setDay(2017, 0, 25, 11, 45);
            TADVI.getS_teacher("ТЛанин");
            TADVI.getS_group("ПИ-16-2");
            NS.setId(3);
            NS.setName("Нейронные сети");
            NS.setDay(2017, 0, 25, 9, 45);
            NS.getS_teacher("Смирнов");
            NS.getS_group("БИ-16-1");

            lectures.add(APNYA);
            lectures.add(APNYA);
            lectures.add(APNYA);
        }
    }
}
