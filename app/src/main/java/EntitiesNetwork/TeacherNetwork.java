package EntitiesNetwork;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import Client.*;
import Entities.*;
import Interacrtors.*;

public class TeacherNetwork {

        private Teacher Smirnov = new Teacher();
        private Teacher Lanin = new Teacher();
        private Teacher Tulyakov = new Teacher();
        private static ArrayList<Teacher> teachers=new ArrayList<Teacher>();

        private TeacherListener delegate;

        public TeacherNetwork(TeacherListener delegate) {
            this.delegate = delegate;
        }


    public void getTeachers(){
            try {
                TimeUnit.SECONDS.sleep(1);
                this.setTeachers();
                delegate.onGetTeachers(teachers);
            }
            catch (InterruptedException ie) { }
            }

    public static Teacher getByName(String name) {
        for (int i = 0; i < teachers.size(); i++) {
            if (teachers.get(i).getName() == name)
            {
                return teachers.get(i);
            }
        }
        return null;
    }

    public void setTeachers(){
        if (teachers.isEmpty()) {

            Smirnov.setName("Смирнов");
            Smirnov.setNumber("88005553535");
            Smirnov.setEmail("smirnovAlex@gmail.com");
            Smirnov.setRoom("415[1]");
            Lanin.setName("Ланин");
            Lanin.setNumber("88005553535");
            Lanin.setEmail("laninvl@hse.ru");
            Lanin.setRoom("315[3]");
            Tulyakov.setName("Туляков");
            Tulyakov.setNumber("88005553535");
            Tulyakov.setEmail("tulyakov@hse.ru");
            Tulyakov.setRoom("304[3]");

            teachers.add(Smirnov);
            teachers.add(Lanin);
            teachers.add(Tulyakov);
        }
    }

}

