package Settings;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;

public class LectureSettings {

    public static final String APP_PREFERENCES = "preferences"; // Имя файла настроек
    public static SharedPreferences sPref; // Объект для работы с параметрами

    final String NAME = "NAME"; // Константа с именем параметра названия пары
    final String TIME = "TIME"; // Константа с именем параметра времени
    final String CABINET = "CABINET"; // Константа с именем параметра кабинета

    String name;
    String time;
    String cabinet;

    // Метод для считывания параметров


    // Метод для считывания параметра "Название"
    public String loadParameterName(Context a) {
        sPref = a.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE); // Инициализация переменной для работы с настройками

        String result = "";

        if (sPref.contains(NAME)) {
            result = sPref.getString(NAME, "");
        }

        return result;
    }

    // Метод для считывания параметра "Время"
    public String loadParameterTime(Context a) {
        sPref = a.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE); // Инициализация переменной для работы с настройками

        String result = "";

        if (sPref.contains(TIME)) {
            result = sPref.getString(TIME, "");
        }

        return result;
    }

    // Метод для считывания параметра "Кабинет"
    public String loadParameterCabinet(Context a) {
        sPref = a.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE); // Инициализация переменной для работы с настройками

        String result = "";

        if (sPref.contains(CABINET)) {
            result = sPref.getString(CABINET, "");
        }

        return result;
    }

    // Метод для сохранения полученной информации в параметры
    public void saveParameters(Context a, String value) {
        sPref = a.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE); // Инициализация переменной для работы с настройками

        ArrayList<String> values = new ArrayList<String>();
        //value.replaceAll("\n"," ");
        String s="";

        for (int i = 0; i < value.length(); i++)
        {
            if (value.charAt(i)=='\n') {
                values.add(s);
                s=" ";
            }
            else {
                s=s+value.charAt(i);
            }
            if (i==(value.length()-1))
                values.add(s);

        }

        SharedPreferences.Editor prefEditor = sPref.edit();

        String n=values.get(0);
        String t=values.get(1);
        String c=values.get(2);

        prefEditor.putString(NAME, n);
        prefEditor.putString(TIME, t);
        prefEditor.putString(CABINET, c);

        prefEditor.commit();
    }

    public void resetParametersRoleAndGroup(Context a) {
        sPref = a.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE); // Инициализация переменной для работы с настройками

        SharedPreferences.Editor prefEditor = sPref.edit();
        prefEditor.putString(NAME, "");
        prefEditor.putString(TIME, "");
        prefEditor.putString(CABINET, "");
        prefEditor.commit();
    }
}
