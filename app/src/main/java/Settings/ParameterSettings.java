package Settings;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class ParameterSettings {
    public static final String APP_PREFERENCES = "preferences"; // Имя файла настроек
    public static SharedPreferences sPref; // Объект для работы с параметрами

    final String ROLE = "ROLE"; // Константа с именем параметра роли
    final String GROUP = "GROUP"; // Константа с именем параметра группы
    final String TEACHER = "TEACHER"; // Константа с именем параметра имени преподавателя

    String role;
    String group;
    String teacher;

    public String loadParameterRole(Context a) {
        sPref = a.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE); // Инициализация переменной для работы с настройками


        String result = "";

        if (sPref.contains(ROLE)) {
            result = sPref.getString(ROLE, "");
        }

        return result;
    }

    // Метод для сохранения полученной роли в параметр
    public void saveParameterRole(Context a, String roleValue) {
        sPref = a.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE); // Инициализация переменной для работы с настройками

        SharedPreferences.Editor prefEditor = sPref.edit();
        prefEditor.putString(ROLE, roleValue);
        prefEditor.commit();
    }

    // Метод для считывания параметра группа
    public String loadParameterGroup(Context a) {
        sPref = a.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE); // Инициализация переменной для работы с настройками

        String result = "";

        if (sPref.contains(GROUP)) {
            result = sPref.getString(GROUP, "");
        }

        return result;
    }

    // Метод для сохранения полученной группы в параметр
    public void saveParameterGroup(Context a, String groupValue) {
        sPref = a.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE); // Инициализация переменной для работы с настройками

        SharedPreferences.Editor prefEditor = sPref.edit();
        prefEditor.putString(GROUP, groupValue);
        prefEditor.commit();
    }

    // Метод для считывания параметра имя преподавателя
    public String loadParameterTeacher(Context a) {
        sPref = a.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE); // Инициализация переменной для работы с настройками

        String result = "";

        if (sPref.contains(TEACHER)) {
            result = sPref.getString(TEACHER, "");
        }

        return result;
    }

    // Метод для сохранения полученного имени преподавателя в параметр
    public void saveParameterTeacher(Context a, String teacherName) {
        sPref = a.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE); // Инициализация переменной для работы с настройками

        SharedPreferences.Editor prefEditor = sPref.edit();
        prefEditor.putString(TEACHER, teacherName);
        prefEditor.commit();
    }

    // Метод для обнуления параметров
    public void resetParametersRoleAndGroup(Context a) {
        sPref = a.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE); // Инициализация переменной для работы с настройками

        SharedPreferences.Editor prefEditor = sPref.edit();
        prefEditor.putString(ROLE, "");
        prefEditor.putString(GROUP, "");
        prefEditor.commit();
    }
}
