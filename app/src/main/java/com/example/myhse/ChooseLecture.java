package com.example.myhse;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;

import Settings.LectureSettings;
import Settings.ParameterSettings;

import Client.GroupListener;
import Client.LectureListener;
import Client.TeacherListener;
import Entities.Group;
import Entities.Lecture;
import Entities.Teacher;
import EntitiesNetwork.GroupNetwork;
import EntitiesNetwork.TeacherNetwork;
import EntitiesNetwork.LectureNetwork;

public class ChooseLecture extends AppCompatActivity implements TeacherListener, GroupListener, LectureListener {

    public static class SubGroup {
        public static String value = "";
    }

    ParameterSettings parameterSettings = new ParameterSettings(); // Создаём экземляр класса parameterSettings
    LectureSettings lectureSettings = new LectureSettings();

    String role;
    String group;
    String teacher;
    TeacherNetwork netTeacher; // Копируем это если нужно
    GroupNetwork netGroup;
    LectureNetwork netLecture;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_lecture);

        lectureSettings.resetParametersRoleAndGroup(this);

        role = parameterSettings.loadParameterRole(this);
        group = parameterSettings.loadParameterGroup(this);
        teacher = parameterSettings.loadParameterTeacher(this);

        final Intent intent = new Intent(this, OneLecture.class);

        setTitle("Расписание");

        TextView text = findViewById(R.id.youAre);

        if (role.equals("Студент")) {
            text.setText(group);
        }
        else
        {
            text.setText(teacher);
        }

        //netLecture = new LectureNetwork(this);
        //netLecture.getLectures();

        ArrayList lessons = new ArrayList();

        lessons.add("Понедельник");
        lessons.add("пара1"+"\n"+"10:00"+"\n"+"3 кабинет");
        lessons.add("Вторник");
        lessons.add("пара2"+"\n"+"12:00"+"\n"+"15 кабинет");
        lessons.add("пара3"+"\n"+"14:00"+"\n"+"22 кабинет");
        lessons.add("Среда");
        lessons.add("пара4"+"\n"+"12:00"+"\n"+"15 кабинет");
        lessons.add("пара5"+"\n"+"14:00"+"\n"+"22 кабинет");
        lessons.add("Четверг");
        lessons.add("пара4"+"\n"+"12:00"+"\n"+"15 кабинет");
        lessons.add("пара5"+"\n"+"14:00"+"\n"+"22 кабинет");
        lessons.add("Пятница");
        lessons.add("пара4"+"\n"+"12:00"+"\n"+"15 кабинет");
        lessons.add("пара5"+"\n"+"14:00"+"\n"+"22 кабинет");
        lessons.add("Суббота");
        lessons.add("Воскресенье");


        final ListView LecturesList = (ListView) findViewById(R.id.lecturesID);

        // создаем адаптеры
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, lessons);


        // присваиваем адаптер списку
        LecturesList.setAdapter(adapter);

        LecturesList.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id)
            {
                String value = (String) LecturesList.getItemAtPosition(position);
                if (value!="Понедельник" && value!="Вторник" && value!="Среда" && value!= "Четверг" && value!="Пятница" && value!="Суббота" && value!="Воскресенье") {
                    lectureSettings.saveParameters(ChooseLecture.this, value);
                    startActivity(intent);
                }
            }
        });


        BottomNavigationView navigationView = findViewById(R.id.bottom_navigation);
        navigationView.setSelectedItemId(R.id.nav_timetable);
        navigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId())
                {
                    case R.id.nav_timetable:
                        Intent a = new Intent(ChooseLecture.this, ChooseLecture.class);
                        startActivity(a);
                        //selectedFragment = new Timetable();
                        break;

                    case R.id.nav_teachers_list:
                        Intent b = new Intent(ChooseLecture.this, choose_teacher.class);
                        startActivity(b);
                        //selectedFragment = new TeachersList();
                        break;

                    case R.id.nav_settings:
                        Intent c = new Intent(ChooseLecture.this, MainActivity.class);
                        startActivity(c);
                        //selectedFragment = new Settings();
                        break;
                }
                return false;
            }
        });
    }



    public void onGetGroups(ArrayList<Group> list) {

    }

    public void onGetTeachers(ArrayList<Teacher> list) {

    }

    public void onGetLectures(ArrayList<Lecture> list) {
        ListView listOfLecture = (ListView) findViewById(R.id.listOfLecture);

        ArrayList<String> lectures = new ArrayList<String>();

        switch (role) {
            case "Студент": {
                for (int i = 0; i < list.size(); i++) {
                   if (list.get(i).getS_group() == group)
                        lectures.add(list.get(i).getName());
                }
            }
            case "Преподаватель": {
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).getS_teacher() == teacher)
                        lectures.add(list.get(i).getName());
                }
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_list_item_1, lectures);

            listOfLecture.setAdapter(adapter);
        }
    }
}
