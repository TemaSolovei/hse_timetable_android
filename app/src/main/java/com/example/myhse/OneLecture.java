package com.example.myhse;
import java.time.Year;
import java.util.Calendar;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import Settings.LectureSettings;

public class OneLecture extends AppCompatActivity {

    String name;
    String time;
    String cabinet;

    int Date;
    int Month;
    int Year;

    LectureSettings lectureSettings = new LectureSettings();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one_lecture);

        Calendar calendar = Calendar.getInstance();

        Date = calendar.get(Calendar.DAY_OF_MONTH);
        Month = calendar.get(Calendar.MONTH);
        Year = calendar.get(Calendar.YEAR);

        String day = Integer.toString(Date)+"/"+Integer.toString(Month)+"/"+Integer.toString(Year);

        setTitle("Информация о паре");

        name = lectureSettings.loadParameterName(this);
        time = lectureSettings.loadParameterTime(this);
        cabinet = lectureSettings.loadParameterCabinet(this);

        TextView nameTextView = findViewById(R.id.Name);
        TextView timeTextVIew = findViewById(R.id.Time);
        TextView cabinetTextView = findViewById(R.id.Cabinet);

        nameTextView.setText(name);
        timeTextVIew.setText(time);
        cabinetTextView.setText(cabinet);

        TextView dateTextView = findViewById(R.id.Date);
        TextView teacherOrGroupTextView = findViewById(R.id.TeacherOrGroup);
        TextView builgingTextView = findViewById(R.id.Building);

        dateTextView.setText(day);
        teacherOrGroupTextView.setText("В. В. Ланин");
        builgingTextView.setText("3 корпус");
    }
}
