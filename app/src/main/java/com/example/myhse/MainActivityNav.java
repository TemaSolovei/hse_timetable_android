package com.example.myhse;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;

import Settings.LectureSettings;
import Settings.ParameterSettings;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivityNav extends AppCompatActivity {

    ParameterSettings parameterSettings = new ParameterSettings(); // Создаём экземляр класса parameterSettings
    LectureSettings lectureSettings = new LectureSettings();

    Button editRoleGroup, timetable;
    String role, group, teacher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_nav);

        BottomNavigationView bottomNav = findViewById(R.id.bottom_navigation);
        bottomNav.setOnNavigationItemSelectedListener(navListener);

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
               new Timetable()).commit();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    Fragment selectedFragment = null;

                    switch (menuItem.getItemId())
                    {
                        case R.id.nav_timetable:

                            //selectedFragment = new Timetable();
                            break;

                        case R.id.nav_teachers_list:
                            //startActivity(new Intent(MainActivityNav.this, choose_teacher.class));
                            selectedFragment = new TeachersList();
                            break;

                        case R.id.nav_settings:
                            //startActivity(new Intent(MainActivityNav.this, choo));
                            selectedFragment = new Settings();
                            break;
                    }

                    if (selectedFragment != null) getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                            selectedFragment).commit();

                    return true;
                }
            };
}
