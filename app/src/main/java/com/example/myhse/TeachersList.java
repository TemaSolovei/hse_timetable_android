package com.example.myhse;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class TeachersList extends Fragment {

    String[] names = {"Лапушка А.Р.", "Божья Воля А.Р.", "Мугамедов А.Р.", "Красилич А.Р.",
            "Мазунин К.К", "Кокорин Ла", "Кузнецов ВВ", "Батыев К К", "Леонов ВВ", "Лодыгин ВВ"};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_teachers_list, container, false);

        // Находим список
        ListView lvMain = rootView.findViewById(R.id.lvMain);

        // Заполняем список
        ArrayList<String> data = new ArrayList<String>();
        data.add("Батыев К К");
        data.add("Божья Воля А.Р.");
        data.add("Кокорин Ла");
        data.add("Красилич А.Р.");
        data.add("Кузнецов ВВ");
        data.add("Лапушка А.Р.");
        data.add("Леонов ВВ");
        data.add("Лодыгин ВВ");
        data.add("Мазунин К.К");
        data.add("Мугамедов А.Р.");


        // Создаём адаптер
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, data);

        //Присваиваем адаптер списку
        lvMain.setAdapter(adapter);


        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_teachers_list, container, false);
    }


}
