package com.example.myhse;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;

import Settings.LectureSettings;
import Settings.ParameterSettings;

import Client.GroupListener;
import Client.LectureListener;
import Client.TeacherListener;
import Entities.Group;
import Entities.Lecture;
import Entities.Teacher;
import EntitiesNetwork.GroupNetwork;
import EntitiesNetwork.TeacherNetwork;
import EntitiesNetwork.LectureNetwork;

public class choose_teacher extends AppCompatActivity implements TeacherListener, GroupListener, LectureListener {

    ParameterSettings parameterSettings = new ParameterSettings(); // Создаём экземляр класса parameterSettings
    LectureSettings lectureSettings = new LectureSettings();

    String role;
    String group;
    String teacher;
    TeacherNetwork netTeacher; // Копируем это если нужно
    GroupNetwork netGroup;
    LectureNetwork netLecture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_teacher);

        lectureSettings.resetParametersRoleAndGroup(this);

        role = parameterSettings.loadParameterRole(this);
        group = parameterSettings.loadParameterGroup(this);
        teacher = parameterSettings.loadParameterTeacher(this);

        final Intent intent = new Intent(this, OneTeacher.class);

        setTitle("Преподаватели");

        netLecture = new LectureNetwork(this);
        netLecture.getLectures();
        netTeacher = new TeacherNetwork(this);
        netTeacher.getTeachers();

        ArrayList teachers1 = new ArrayList();
        ArrayList teachers2 = new ArrayList();
        ArrayList teachers3 = new ArrayList();
        ArrayList teachers4 = new ArrayList();

        teachers1.add("Батыев К К");
        teachers1.add("Божья Воля А.Р.");
        teachers2.add("Кокорин Ла");
        teachers2.add("Красилич А.Р.");
        teachers2.add("Кузнецов ВВ");
        teachers3.add("Лапушка А.Р.");
        teachers3.add("Леонов ВВ");
        teachers3.add("Лодыгин ВВ");
        teachers4.add("Мазунин К.К");
        teachers4.add("Мугамедов А.Р.");


        final ListView firstList = (ListView) findViewById(R.id.listOfLecture);
        final ListView secondList = (ListView) findViewById(R.id.secondListOfLecture);
        final ListView thirdList = (ListView) findViewById(R.id.thirdListOfLectures);


        // создаем адаптеры
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, teachers1);
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, teachers2);
        ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, teachers3);
        ArrayAdapter<String> adapter4 = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, teachers4);



        // присваиваем адаптер списку
        firstList.setAdapter(adapter1);
        secondList.setAdapter(adapter2);
        thirdList.setAdapter(adapter3);

        firstList.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id)
            {
                /*String value = (String) firstList.getItemAtPosition(position);
                lectureSettings.saveParameters(choose_teacher.this, value);*/
                startActivity(intent);
            }
        });

        secondList.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id)
            {
                /*String value = (String) secondList.getItemAtPosition(position);
                lectureSettings.saveParameters(choose_teacher.this, value);*/
                startActivity(intent);
            }
        });

        thirdList.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id)
            {
               /* String value = (String) thirdList.getItemAtPosition(position);
                lectureSettings.saveParameters(choose_teacher.this, value); */
                startActivity(intent);
            }
        });

        BottomNavigationView navigationView = findViewById(R.id.bottom_navigation);
        navigationView.setSelectedItemId(R.id.nav_teachers_list);
        navigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId())
                {
                    case R.id.nav_timetable:
                        Intent a = new Intent(choose_teacher.this, ChooseLecture.class);
                        startActivity(a);
                        //selectedFragment = new Timetable();
                        break;

                    case R.id.nav_teachers_list:
                        Intent b = new Intent(choose_teacher.this, choose_teacher.class);
                        startActivity(b);
                        //selectedFragment = new TeachersList();
                        break;

                    case R.id.nav_settings:
                        Intent c = new Intent(choose_teacher.this, MainActivity.class);
                        startActivity(c);
                        //selectedFragment = new Settings();
                        break;
                }
                return false;
            }
        });
    }

    public void onGetGroups(ArrayList<Group> list) {

    }

    public void onGetTeachers(ArrayList<Teacher> list) {
        ListView listOfLecture = (ListView) findViewById(R.id.listOfLecture);

        ArrayList<String> teachers = new ArrayList<String>();

        for (int i = 0; i < list.size(); i++) {
            teachers.add(list.get(i).getName());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, teachers);

        listOfLecture.setAdapter(adapter);
    }

    public void onGetLectures(ArrayList<Lecture> list) {
        ListView listOfLecture = (ListView) findViewById(R.id.listOfLecture);

        ArrayList<String> lectures = new ArrayList<String>();

        switch (role) {
            case "Студент": {
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).getS_group() == group)
                        lectures.add(list.get(i).getName());
                }
            }
            case "Преподаватель": {
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).getS_teacher() == teacher)
                        lectures.add(list.get(i).getName());
                }
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_list_item_1, lectures);

            listOfLecture.setAdapter(adapter);
        }
    }
}
