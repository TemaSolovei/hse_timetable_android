package com.example.myhse;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;

import Client.GroupListener;
import Client.LectureListener;
import Client.TeacherListener;
import Entities.Group;
import Entities.Lecture;
import Entities.Teacher;
import EntitiesNetwork.LectureNetwork;
import Settings.LectureSettings;
import Settings.ParameterSettings;

/**
 * A simple {@link Fragment} subclass.
 */
public class Timetable extends Fragment {

    ParameterSettings parameterSettings = new ParameterSettings(); // Создаём экземляр класса parameterSettings
    LectureSettings lectureSettings = new LectureSettings();

    String role, group, teacher;

    public Timetable() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_timetable, container, false);

        lectureSettings.resetParametersRoleAndGroup(rootView.getContext());

        role = parameterSettings.loadParameterRole(rootView.getContext());
        group = parameterSettings.loadParameterGroup(rootView.getContext());
        teacher = parameterSettings.loadParameterTeacher(rootView.getContext());

        final Intent intent = new Intent(rootView.getContext(), OneLecture.class);// !!!!!

        TextView text = rootView.findViewById(R.id.youAre);

        if (role.equals("Студент")) {
            text.setText(group);
        }
        else
        {
            text.setText(teacher);
        }

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_timetable, container, false);
    }
}
