package com.example.myhse;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.BitSet;

import Settings.LectureSettings;
import Settings.ParameterSettings;

/**
 * A simple {@link Fragment} subclass.
 */
public class Settings extends Fragment {

    ParameterSettings parameterSettings = new ParameterSettings(); // Создаём экземляр класса parameterSettings
    LectureSettings lectureSettings = new LectureSettings();

    Button editRoleGroup, timetable;
    String role, group, teacher;

    public Settings() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_settings, container, false);

        lectureSettings.resetParametersRoleAndGroup(rootView.getContext());

        role = parameterSettings.loadParameterRole(rootView.getContext());
        group = parameterSettings.loadParameterGroup(rootView.getContext());
        teacher = parameterSettings.loadParameterTeacher(rootView.getContext());

        if (((role.equals("")) && (group.equals("")))||//если имя препода или студента =0, то
                ((role.equals(null)) && (group.equals(null)))) {
            Intent intent = new Intent(rootView.getContext(), ChooseRole.class); //запустить активность выбор роли
            startActivity(intent);
        }

        TextView roleTextView = rootView.findViewById(R.id.mainRole); //находит id с ролью
        TextView textGroup = rootView.findViewById(R.id.mainYourGroup);
        TextView groupTextView = rootView.findViewById(R.id.mainGroup);

        roleTextView.setText(role);

        if (role.equals("Студент")) {
            textGroup.setText("Ваша группа:");
            groupTextView.setText(group);
        }
        else
        {
            textGroup.setText("Ваше имя:");
            groupTextView.setText(teacher);
        }

        Button editButton = (Button) rootView.findViewById(R.id.editRoleGroup);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parameterSettings.resetParametersRoleAndGroup(rootView.getContext());

                Intent intent = new Intent(rootView.getContext(), ChooseRole.class);
                startActivity(intent);
            }
        });

        // Inflate the layout for this fragment
        return rootView;
    }
}
