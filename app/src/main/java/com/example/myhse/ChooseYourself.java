package com.example.myhse;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import Client.GroupListener;
import Client.TeacherListener;
import Entities.Group;
import Entities.Teacher;
import EntitiesNetwork.GroupNetwork;
import EntitiesNetwork.TeacherNetwork;
import Settings.ParameterSettings;

public class ChooseYourself extends AppCompatActivity implements TeacherListener, GroupListener {

    //public static class SubGroup { public static String value = ""; } //класс-костыль для создания глобальной переменой с выбранной группой или именем препода

    ParameterSettings parameterSettings = new ParameterSettings(); // Создаём экземляр класса parameterSettings

    String role;
    TeacherNetwork netTeacher; // Копируем это если нужно
    GroupNetwork netGroup;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_yourself);

        role = parameterSettings.loadParameterRole(this);

        setTitle("Выберите тип расписания:");

        TextView text = findViewById(R.id.chooseYourselfText);
        final Intent intent = new Intent(this, MainActivityNav.class);

        switch (role) {
            case "Студент": {
                text.setText("Выберите группу:");
                netGroup = new GroupNetwork(this);
                netGroup.getGroups();

                break;
            }

            case "Преподаватель": {
                text.setText("Выберите имя:");
                netTeacher = new TeacherNetwork(this);
                netTeacher.getTeachers();

                break;
            }
        }

        final ListView list = (ListView)findViewById(R.id.listOfPersons);

        //адаптер
        list.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id)
            {
                String groupNameValue = (String) list.getItemAtPosition(position);

                if (role.equals("Студент")) {
                    parameterSettings.saveParameterGroup(ChooseYourself.this, groupNameValue);
                } else
                {
                    parameterSettings.saveParameterTeacher(ChooseYourself.this, groupNameValue);
                }

                startActivity(intent);
            }
        });
    }

    public void onGetGroups(ArrayList<Group> list) {
        // Прописать тут чтобы лоадер перестал показываться
        ListView listOfPersons = (ListView) findViewById(R.id.listOfPersons);

        ArrayList<String> groups = new ArrayList<String>();

        for (int i = 0; i < list.size(); i++)
        {
            groups.add(list.get(i).getName());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, groups);

        listOfPersons.setAdapter(adapter);
    }
    public void onGetTeachers(ArrayList<Teacher> list) {
        // Прописать тут чтобы лоадер перестал показываться
        ListView listOfPersons = (ListView) findViewById(R.id.listOfPersons);

        String[] teachers = new String[list.size()];

        for (int i = 0; i < list.size(); i++)
        {
            teachers[i] = list.get(i).getName();
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, teachers);

        listOfPersons.setAdapter(adapter);
    }
}
