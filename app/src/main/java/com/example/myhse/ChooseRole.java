package com.example.myhse;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.content.SharedPreferences.Editor;

import Settings.ParameterSettings;

public class ChooseRole extends AppCompatActivity {

    ParameterSettings parameterSettings; // Создаём экземляр класса parameterSettings

    //public static class Role { public static String value = ""; }

        @Override
    protected void onCreate(Bundle savedInstanceState) {
        parameterSettings = new ParameterSettings();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_role);

        setTitle("Выберите тип расписания:"); //устанавливает заголовок
    }

    // Обрабатываем выбор роли
    public void checkRole(View view) {
        Button pressedButton = (Button) view;
        String roleValue = pressedButton.getText().toString();

        TextView resultRole = findViewById(R.id.roleResult);
        parameterSettings.saveParameterRole(this, roleValue);

        String result = parameterSettings.loadParameterRole(this);
        resultRole.setText("Ваша роль: " + result);

        Intent intent = new Intent(this, ChooseYourself.class);
        startActivity(intent);
    }
}
