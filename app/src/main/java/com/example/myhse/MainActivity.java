package com.example.myhse;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.service.chooser.ChooserTargetService;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import Settings.LectureSettings;
import Settings.ParameterSettings;

public class MainActivity extends AppCompatActivity {

    Button editRoleGroup;
    Button timetable;

    ParameterSettings parameterSettings = new ParameterSettings(); // Создаём экземляр класса parameterSettings
    LectureSettings lectureSettings = new LectureSettings();

    String role;
    String group;
    String teacher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        lectureSettings.resetParametersRoleAndGroup(this);

        role = parameterSettings.loadParameterRole(this);
        group = parameterSettings.loadParameterGroup(this);
        teacher = parameterSettings.loadParameterTeacher(this);


        if (((role.equals("")) && (group.equals("")))||//если имя препода или студента =0, то
                ((role.equals(null)) && (group.equals(null)))) {
            Intent intent = new Intent(this, ChooseRole.class); //запустить активность выбор роли
            startActivity(intent);
        }

        super.onCreate(savedInstanceState); //генеряться сами
        setContentView(R.layout.activity_main2);

        TextView roleTextView = findViewById(R.id.mainRole); //находит id с ролью
        TextView textGroup = findViewById(R.id.mainYourGroup);
        TextView groupTextView = findViewById(R.id.mainGroup);

        roleTextView.setText(role);

        if (role.equals("Студент")) {
            textGroup.setText("Ваша группа:");
            groupTextView.setText(group);
        }
        else
        {
            textGroup.setText("Ваше имя:");
            groupTextView.setText(teacher);
        }

        editRoleGroup = (Button) findViewById(R.id.editRoleGroup);
        timetable = (Button) findViewById(R.id.timetable);

        BottomNavigationView navigationView = findViewById(R.id.bottom_navigation);
        navigationView.setSelectedItemId(R.id.nav_settings);
        navigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId())
                {
                    case R.id.nav_timetable:
                        Intent a = new Intent(MainActivity.this, ChooseLecture.class);
                        startActivity(a);
                        //selectedFragment = new Timetable();
                        break;

                    case R.id.nav_teachers_list:
                        Intent b = new Intent(MainActivity.this, choose_teacher.class);
                        startActivity(b);
                        //selectedFragment = new TeachersList();
                        break;

                    case R.id.nav_settings:
                        Intent c = new Intent(MainActivity.this, MainActivity.class);
                        startActivity(c);
                        //selectedFragment = new Settings();
                        break;
                }
                return false;
            }
        });
    }

    // Кнопочка "Изменить"
    public void onClickEditRoleGroup(View view)
    {
        parameterSettings.resetParametersRoleAndGroup(this);

        Intent intent = new Intent(this, ChooseRole.class);
        startActivity(intent);
    }

    // Кнопочка "Расписание"
    public void onClickTimetable(View view) {
        Intent intent = new Intent(this, ChooseLecture.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }


}
