package Client;

import java.util.ArrayList;

import Entities.*;

public interface GroupListener {
    void onGetGroups(ArrayList<Group> list);
}
