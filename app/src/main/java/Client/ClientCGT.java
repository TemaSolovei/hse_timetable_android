package Client;

import java.util.ArrayList;

import EntitiesNetwork.*;
import Entities.*;

// Обязательно писать implements
public class ClientCGT implements TeacherListener, LectureListener, GroupListener {

    TeacherNetwork netTeacher; // Копируем это если нужно
    LectureNetwork netLecture;
    GroupNetwork netGroup;

    void createTeacherList() {
        netTeacher = new TeacherNetwork(this);
        netTeacher.getTeachers();
    }

    void createLectureList() {
        netLecture = new LectureNetwork(this);
        netLecture.getLectures();
    }

    void createGroupList() {
        netGroup = new GroupNetwork(this);
        netGroup.getGroups();
    }

    // При имплементации прописывать это
    public void onGetTeachers(ArrayList<Teacher> list) { }

    public void onGetLectures(ArrayList<Lecture> list) { }

    public void onGetGroups(ArrayList<Group> list) { }
}
