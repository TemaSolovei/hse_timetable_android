package Client;

import java.util.ArrayList;

import Entities.Lecture;

public interface LectureListener {
    void onGetLectures(ArrayList<Lecture> list);
}
