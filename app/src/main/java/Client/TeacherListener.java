package Client;

import com.example.myhse.ChooseRole;

import java.util.ArrayList;

import Entities.Teacher;


public interface TeacherListener {
    void onGetTeachers(ArrayList<Teacher> list);

}