package Entities;

//это класс пары

import java.sql.Time;
import java.util.Calendar;
import java.util.Date;
import Entities.*;

public class Lecture {
    private int id;
    private String name;
    private String s_teacher;
    private String s_group;
    private Calendar date;
    private Teacher teacher;
    private Group group;

    public int getId() {return id;};

    public String getName()
    {
        return name;
    }

    public String getS_teacher()
    {
        return name;
    }

    public String getS_group()
    {
        return name;
    }

    public Calendar getDate() { return date; }

    public Teacher getTeacher() {return teacher;}

    public Group getGroup() {return group;}

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void getS_teacher(String s_teacher) {
        this.s_teacher= s_teacher;
    }

    public void getS_group(String s_group) {
        this.s_group= s_group;
    }

    public void setDay(int year, int month, int d, int hour, int minut) {

        this.date.set(year, month, d, hour, minut);
    }

    public void setTeacher(Teacher teacher)
    {
        this.teacher=teacher;
    }

    public void setGroup(Group group)
    {
        this.group=group;
    }
}

